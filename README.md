# SQL Exercises

You have been doing lot's of SQL - This is great! Here, have a :taco: and a :beer:

Now it's time to get some practice in!

### Pre Requesist

- PostGres or Mysql 
- For exercises around queering DML - youll need to  download & load the sample data in Chinook Database https://github.com/lerocha/chinook-database go to: chinook-database/ChinookDatabase/DataSources/


### The exercises

The exercises focus around:

- DDL - Creating Databases, creating schemas and Relationship Entity Diagram from user stories
- DML - focus on add and alter data
- DML - focus on queering the data

### SQL topic

This is a list of some of the SQL topics for your to write notes agains or go research:

**Intro to DB and SQL**

- What is a DB
- What is SQL?
- Primary Keys and Foreign keys
- SQL relationships: 1-1, 1-N, N-N
- SQL best practices - 1st, 2nd and 3rd normal form - Basically don't repeat yoursel and make things atomic
- Relationship Entity Diagram from relationships 
  

**DDL**

- Relationship Entity Diagram from relationships
- Datatypes
- Commands to create tables and column 
- Setting primary key and making foreing keys
- Constraints

**DML**

- Inserting data
- Altering data
- Removing data 
  

**DML part 2**

- Queries
- Agregate functions
- Joint Queries
- Subqueries
- Aliasing collumn and other

**DCL and TCL**

- Making users
- Permission 
- Role backs and backups 
