# EBooks.you - Books you write and read! Exercis 2 :books:

Amazing, just amazing! You have made a DB. 

What tables do you have? How have you figured everything out?

- Users
- Books
- Sales

Let's start at the beggining.

#### Users Table

We'll need a user table! They will need a Primary Key, lets just said ID.

|   | user_ID (PK) |   |   |   |   |
|---|--------------|---|---|---|---|
|   |              |   |   |   |   |
|   |              |   |   |   |   |
|   |              |   |   |   |   |


We also figured out the user will need:

- Name
- email
- phone number 

We should probably devide the name into first_name and last_name. This will keep our information atomic.

|   | user_ID (PK) | f_name | l_name | email | phone |
|---|--------------|--------|--------|-------|-------|
|   |              |        |        |       |       |
|   |              |        |        |       |       |
|   |              |        |        |       |       |

All of the above should be store as text VARCHAR. 

#### Books Table

Now.. Books don't exist by them selves. They have to be writen. 

A book belongs to a user. 

A User can have (write) many books. But a book belongs to a use.

This is 1-N relationships. So we'll need a foreing key from the user's table and it will still have it's own id. 

|   | book_ID (PK) | user_id (FK) |   |   |   |
|---|--------------|--------------|---|---|---|
|   |              |              |   |   |   |
|   |              |              |   |   |   |
|   |              |              |   |   |   |


What else is in books? 

- Title
- Content
- date of publish


|   | book_ID (PK) | user_id (FK) | title | content | date_publish |
|---|--------------|--------------|-------|---------|--------------|
|   |              |              |       |         |              |
|   |              |              |       |         |              |
|   |              |              |       |         |              |



#### Sales Table

Now.. We have users we have books. Let's make some sales. 

We'll need to identify the sale buy it's unique PK.

|   | sale_id |   |   |   |   |
|---|---------|---|---|---|---|
|   |         |   |   |   |   |
|   |         |   |   |   |   |
|   |         |   |   |   |   |


Sales.. Are done by a user, the buyer.

So a sale ALWAYS belongs to a user, who is the buyer. We'll need that forign key.

|   | sale_id | user_id (FK) |   |   |   |
|---|---------|--------------|---|---|---|
|   |         |              |   |   |   |
|   |         |              |   |   |   |
|   |         |              |   |   |   |

What else.. We need the buyer, and the book being sold... AT LEAST. 

So we'll need the primary key of the book being sold to ALSO so be forign key in this table. 

So a sale, belong to a user (the buyer) and belong to a book (item sold). This is what's known as N-N relationhsip. 

|   | sale_id | user_id (FK) | book_id (fk) |   |   |
|---|---------|--------------|--------------|---|---|
|   |         |              |              |   |   |
|   |         |              |              |   |   |
|   |         |              |              |   |   |

What else.. 

What about the person who get the money? The writer? 
GREAT question!! We know them through the book_id which belongs to a user. 

What about Price? 

Well though.. Also via the book_id! no need to duplicate data. Add that to the table of the books ;) 

Maybe a date.. let's add a date.

|   | sale_id | user_id (FK) | book_id (fk) | date_sale |   |
|---|---------|--------------|--------------|-----------|---|
|   |         |              |              |           |   |
|   |         |              |              |           |   |
|   |         |              |              |           |   |

Done. 

Now Write the code to write the tables! 

Don't forget to add the price to the book table! 

## Task

If you've done and validated the adove, add soem fake data.

10 users
5 books 
3 sales 

Done.
