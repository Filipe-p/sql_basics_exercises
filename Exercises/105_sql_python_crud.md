# EBooks.you - SeedCapital

NEEEEEIICCCCEEEEEE!! or wait... is it... NEEEEEEEPPHEEWWWWWW!!!!!

Just come here! My GEM! My LIFE, My heart! You'll not believe this.


### **I JUST GOT SEED FUNDING**


### ISN't that amazing? All MY hard work

.. What? No you can't have a new computer... what's wrong with the one your mother gave you? ... What am I to you? a bank?

Look - It looks like there might be something in for you - But right now there is nothing. We can't divide nothing - You'll break the universe if you divide by zero :D hahah --- yes, yes.. You told me that.

### Task

Look we got to get out MVP going.
This is the layout of the task:

1. First write the CRUD in SQL
2. Connect Python and your SQL - Try to get all books so you know it works
3. Use functional programing or OOP to write CRUD methods or functions for each table
4. Make a wrapper to have a small demo of you CRUDing the DB

Here are some note I copied in my Entreperneuship class. 

##### **Focus on the 20/80 rule**

This is called Pareto Principle - You can look it up. 20% of your clients will give you 80% of your revenue. And 20% of your code will have 80% of bugs.

And 20% of code will provide 80% of the functionality. We need it to **CRUD**


##### 1) **CRUD** Creat Read Update and Delete

Look at your exercise 103. You have done some Reads.
For the books table let's make it crud!

have the SQL for these:

- READ one book
- READ all books
- Creat a Book
- Update a Book (one)
- Delete a Book (one)

Do the same with users:

- READ one user
- READ all users (one)
- Creat a user (one)
- Update a user (one)
- Delete a user (one)

##### 2) Connecting python and SQL

Cool. Now that you have that you'll need to have a python program to connect to mySQL server.

Re search how to use mysql and python connecto here: 

- https://www.w3schools.com/python/python_mysql_getstarted.asp
- https://dev.mysql.com/doc/connector-python/en/connector-python-example-connecting.html


Right a small program that connects to SQL using python and get's all books.

##### 3) Implement CRUD with python

Use functional programing of OOP to write the CRUD methods for Books and USErs.

You already have the SQL code. Now you need to use it with the mysql python connector

##### 4) Wrap the code

Choose one:

- Create a simple while loop to ask you for data and CRUP
- use python Flask to serve you DB as an API