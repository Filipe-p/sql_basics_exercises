# EBooks.you - Books you write and read! :books:

AHH my brights start in family! My sweat nice/nephew, come here :D how long has it been? Let me tell you have this great IDEA! AND WE?LL BE RICH! RICH I TELL YOU!!

What you ask? Well.. We're going to make an EBOOK STORE! Isn't it brilliant??!

Sit down and Hear me out. 

It's an online store where anyone can register, write a book and post it online! Similarly - Anyone can rigister and just rent out a book! It's genius, BRILLIANT! We'll be THE PLACE for books!

And I heard you know SQL - Don't know what that is, but I've been told I need a DB for my books and sql is they way. 

(...)

WHAT? you just had an intensive course on SQL?! How crazy is this! We'll be rich in no time!

(...) **Your uncle continues to rant**
(...) The next day, after a night of heavy praising, possibly the cajual family bullying, you manage to put together some specs from what your uncle want's to build. 

...

No.. You don't think this will make you rich.. but he has some cash and you could use it to move out of your current room. 

Time to put these tech skill to use. 

## User Stories 

#### User story #1

As a new-user, I want to be able to register with my name, email and phone number, so that I can use the website to both rent books and write books.

#### User story #2

As a user, write a book with a title, some amazing story, price we want to sell it and it should also have a date of publish. I want to do this, so that I can sell it after.

#### User story #3

As a user, I want to be able to buy out a book, so that I can read it. 

The recepit should include information on the sale:
- What book
- Who bought it 
- date of transaction


## Task

Right. We need to start with a DB. Best we make and Entety relationship diagram so we know what table we need, where primary keys go and foreing keys. Also let's add the datatypes and what data we'll keep. 

Let's go! 

**hints** 

- What are the primary tables? 
- what information does each table need?
- What are the 1-N relationship? Does something belong to something else? 
- What are the N-N relationship? 


